# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/yukin/dev/bingfeatures/LibLinear/blas/daxpy.c" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/LibLinear/blas/daxpy.c.o"
  "/home/yukin/dev/bingfeatures/LibLinear/blas/ddot.c" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/LibLinear/blas/ddot.c.o"
  "/home/yukin/dev/bingfeatures/LibLinear/blas/dnrm2.c" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/LibLinear/blas/dnrm2.c.o"
  "/home/yukin/dev/bingfeatures/LibLinear/blas/dscal.c" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/LibLinear/blas/dscal.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/yukin/dev/bingfeatures/LibLinear/linear.cpp" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/LibLinear/linear.cpp.o"
  "/home/yukin/dev/bingfeatures/LibLinear/tron.cpp" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/LibLinear/tron.cpp.o"
  "/home/yukin/dev/bingfeatures/Objectness/CmFile.cpp" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/Objectness/CmFile.cpp.o"
  "/home/yukin/dev/bingfeatures/Objectness/CmShow.cpp" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/Objectness/CmShow.cpp.o"
  "/home/yukin/dev/bingfeatures/Objectness/DataSetVOC.cpp" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/Objectness/DataSetVOC.cpp.o"
  "/home/yukin/dev/bingfeatures/Objectness/FilterTIG.cpp" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/Objectness/FilterTIG.cpp.o"
  "/home/yukin/dev/bingfeatures/Objectness/Main.cpp" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/Objectness/Main.cpp.o"
  "/home/yukin/dev/bingfeatures/Objectness/Objectness.cpp" "/home/yukin/dev/bingfeatures/CMakeFiles/test_objectness.dir/Objectness/Objectness.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "//include/opencv"
  "//include"
  "."
  "/usr/include/vtk"
  "/usr/include/libxml2"
  "/usr/include/freetype2"
  "/usr/include/python2.7"
  "/usr/include/opencv"
  "/usr/include/ni"
  "/usr/lib/x86_64-linux-gnu/glib-2.0/include"
  "/usr/include/glib-2.0"
  "/usr/lib/glib-2.0/include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
